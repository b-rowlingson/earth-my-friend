library(sf)

gp = "../earthmyfriend.gpkg"

points = st_read(gp, "locations")
# points = points[order(points$seq),]
points = points[,c("book_name","found_name","arrival_day","depart_day","page")]

route = st_as_sf(
    data.frame(geometry=st_sfc(st_linestring(st_coordinates(points)), crs=st_crs(points)))
    )

library(leaflet)

nr = paste("<tr><td colspan='2'><b>",points$book_name,"</b></td></tr>",sep="")
arr = ifelse(is.na(points$arrival_day), "", paste0("<tr><td>Arrive:</td><td>",as.character(points$arrival_day),"</td></tr>"))
dep = ifelse(is.na(points$depart_day), "", paste0("<tr><td>Leave:</td><td>",as.character(points$depart_day),"</td></tr>"))
page = ifelse(is.na(points$page), "", paste0("<tr><td>Page:</td><td>",as.character(points$page),"</td></tr>"))
found = ifelse(is.na(points$found_name), "", paste0("<tr><td>Found:</td><td>",as.character(points$found_name),"</td></tr>"))

trs = paste(nr, found, arr, dep, page, sep="")

popup = paste("<table>",
              trs,
              "</table>",sep="")

m = leaflet() %>%
    addPolylines(data=route, dashArray=c(10,10), color="#444", weight=1 ) %>%
    addCircleMarkers(
                      data=points,
                      label=points$book_name,
                      radius = 5,
                      color="black",
                      stroke=FALSE,
                      fillOpacity=1,
                      popup=popup
                      )  %>%
    addTiles()
m


library(htmlwidgets)
saveWidget(m, file="public/m.html")
